The goal of this project is to provide dashboards so as to display data from sensors spread out into the city of La Mure.
To achieve it, we have used Grafana, InfluxDB and the LoRaWAN networking protocol.


**20/01**

Attribution et découverte du sujet : Tableau de bord des capteurs LoRaWAN de la Ville de La Mure




**27/01**

Suivi du cours présentant la technologie LoRaWAN

Découverte des types de capteurs qui vont être utilisés par la ville de La Mure ainsi que les differents objets connectés dont les données vont être collectées.




**03/02**

Lecture des documents relatifs au projet (video,slides,lora.campusiot.imag.fr)

On a reçu un capteur de type mfc88 dans le but de collecter ses données ,les décoder et les afficher dans un Dashboard Graffana .




**10/02**

Lecture des codes en JavaScript présents sur la plateforme LoRa Server dans le but de décoder les infos du capteur.

Afin de pouvoir récupérer les informations envoyées par le capteur, attribution d’un numéro à ce dernier grâce à la technologie NFC (Near Fiels Communication). Cette dernière permet d’établir une connexion entre deux appareils compatibles à courte distance (10 cm maximum) sans la moindre configuration.
Observation du contenu des paquets envoyés par le capteur.



**17/02**

Rappel du schéma liant les capteurs au dashboard.
Parcours de https://lamure.iot.imag.fr pour comprendre comment sont modélisées les informations ainsi que la façon dont elles sont présentées aux utilisateurs. Nous avons observé comment les requêtes sont construites.

Comme la ville de La Mure n’a pas encore reçu les capteurs qu’elle a commandés, nous allons utiliser les informations obtenues à l’aide d’autres capteurs de même type pour tester l’affichage sous Grafana ainsi que le décodage.



**02/03**

Familiarisation avec les plugins offerts par Grafana. Nous avons permis la visualisation des données mesurées par le capteur MCF88_LW12CO2, car il possède de nombreux types de données à afficher séparément sous différentes formes.




**03/03**

Lecture de la documentation à propos des alertes sur Grafana. Apprentissage de la mise en place d'une alerte et d'un dashboard utilisant une Wind Rose (pour comparer les angles d'inclinaison d'un capteur). 
Étude des différentes fonctionnalités d'affichage de Grafana, notamment la manière d'afficher sur un même dashboard des graphiques provenant de différents appareils.




**09/03**

Soutenance de mi-projet




**10/03**

La plupart des capteurs ont été commandés par la Ville de La Mure, ils vont donc arriver sous peu, et des vrais tests avec ces derniers pourront alors être conduits. D’ici-là, à nous de faire en sorte que nous soyons en possession d’un code permettant de décoder les messages de chacun de ses capteurs !
Tâches à effectuer :
 • Dénicher les décodeurs manquants (code qui permet de décoder les messages selon le type de capteur).
 • Lister l’ensemble des plugins intéressants
 • Tester les décodeurs déjà en notre possession pour vérifier qu’ils fonctionnent correctement




**16/03**

Recherche des meilleurs plugins pour afficher les données.




**17/03**

Début des tests de différents décodeurs.




**23/03 && 24/03**

Avancement des tests dans le but de vérifier que les décodeurs transmis fonctionnement correctement. Parfois ajout de tests supplémentaires pour vérifier certains fonctionnalités.




**30/03**

Réunion avec notre partenaire (Mairie de La Mure) pour échanger et faire le point sur le projet.
Création de dashboards où les capteurs sont triés par localisation dans le but de les placer plus tard sur une carte, lorsque nous aurons un emplacement plus précis pour chacun d'eux.




**31/03**

Poursuite de la création de dashboards en fonction de la position des capteurs dans la ville.




**06/04**

Ajout de cartes de La ville de La Mure dans certains dashboards pour y afficher les mesures des capteurs en fonction de l'emplacement de ces derniers sur la carte. 
Difficulté rencontrée sur le placement des données sur la carte, il faut faire un drag pour tenter d'y arriver (protocole pas très intuitif).




**13/04**

Création de dashboards par thématique pour pouvoir comparer plus simplement certaines données mesurées dans des lieux différents.




**20/04**

Continuité de réalisation des exemples de tableaux de bord pour tous les capteurs.




**27/03**

Rédaction et Ajout du Compte-Rendu.
